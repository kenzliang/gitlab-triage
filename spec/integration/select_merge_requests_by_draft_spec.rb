# frozen_string_literal: true

require 'spec_helper'

describe 'select merge requests by draft' do
  include_context 'with integration context'

  let(:draft_mr) do
    mr.merge(title: 'Draft: Hello world')
  end
  let(:non_draft_mr) do
    mr.merge(title: 'Hello World')
  end

  describe 'draft: true' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, wip: 'yes' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [draft_mr]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  draft: true
                actions:
                  comment: |
                    Comment because MR is in draft.
      YAML

      stub_post_draft_mr = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{draft_mr[:iid]}/notes",
        body: { body: 'Comment because MR is in draft.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_draft_mr)
    end
  end

  describe 'draft: false' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests",
        query: { per_page: 100, wip: 'no' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [non_draft_mr]
      end
    end

    it 'comments on the merge request' do
      rule = <<~YAML
        resource_rules:
          merge_requests:
            rules:
              - name: Rule name
                conditions:
                  draft: false
                actions:
                  comment: |
                    Comment because MR is not in draft.
      YAML

      stub_post_non_draft_mr = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/merge_requests/#{non_draft_mr[:iid]}/notes",
        body: { body: 'Comment because MR is not in draft.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_non_draft_mr)
    end
  end
end
