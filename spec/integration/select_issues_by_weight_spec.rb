# frozen_string_literal: true

require 'spec_helper'

describe 'select issues by weight' do
  include_context 'with integration context'

  let!(:issue_with_weight_none) do
    issue.merge(weight: nil)
  end
  let!(:issue_with_weight_1) do
    issue.merge(iid: issue[:iid] * 2, weight: 1)
  end
  let!(:issue_with_weight_3) do
    issue.merge(iid: issue[:iid] * 3, weight: 3)
  end

  describe 'weight of none' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100, weight: 'None' },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [issue_with_weight_none]
      end
    end

    it 'comments on the issue' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Test rule
                conditions:
                  weight: None
                actions:
                  comment: |
                    Comment because its weight is not set.
      YAML

      stub_post_issue_with_weight_none = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_with_weight_none[:iid]}/notes",
        body: { body: 'Comment because its weight is not set.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_issue_with_weight_none)
    end
  end

  describe 'weight of 3' do
    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100, weight: 3 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        [issue_with_weight_3]
      end
    end

    it 'comments on the issue' do
      rule = <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Test rule
                conditions:
                  weight: 3
                actions:
                  comment: |
                    Comment because its weight is 3.
      YAML

      stub_post_issue_with_weight_3 = stub_api(
        :post,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues/#{issue_with_weight_3[:iid]}/notes",
        body: { body: 'Comment because its weight is 3.' },
        headers: { 'PRIVATE-TOKEN' => token })

      perform(rule)

      assert_requested(stub_post_issue_with_weight_3)
    end
  end
end
